<?php 

class Helper
{
    public function response($res, $data, $err_msg = "", $suc_msg = "")
    {
        $data = $data == "" ? [] : $data;
        return 
        array(
            'success' => $res,
            'data' => $data,
            'message' => array(
                "error" => $err_msg,
                "success" => $suc_msg
            )
        );
    }

    public function is_new($id)
    {
        return $id == "" || $id == 0 || $id == null;
    }

    public function get_now()
    {
        return date("Y-m-d H:i:s");
    }
}