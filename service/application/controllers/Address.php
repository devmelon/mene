<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Address extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    /**
     * <Method:>
     *  GET
     * </Method:>
     * <Name:>
     *  GetAddress
     * </Name:>
     * <Parameters:>
     *  Id
     * </Parameters:>
     */
    function GetAddress_get()
    {
        $id = $this->get("Id");

        $sql = "SELECT * FROM Address WHERE Id = $id AND RecordStatusId = 0";

        $data = $this->db->query($sql)->result();

        $this->response(array(
            'message' => true,
            'data' => $data,
            'err' => mysql_error()
        ), 200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  UpdateAddress
     * </Name:>
     * <Parameters:>
     *  Id
     *  TenantId
     *  Address1
     *  Address2
     *  PhoneNumber
     *  City
     *  Province
     *  Country
     *  Zipcode
     *  RecordStatusId
     *  CreatedOn
     *  CreatedBy
     *  UpdatedOn
     *  UpdatedBy
     * </Parameters:>
     */
    function UpdateAddress_post()
    {
        $id = $this->post("Id");
        $data = array(
            "TenantId"       => $this->post("TenantId"),
            "Address1"       => $this->post("Address1"),
            "Address2"       => $this->post("Address2"),
            "PhoneNumber"    => $this->post("PhoneNumber"),
            "City"           => $this->post("City"),
            "Province"       => $this->post("Province"),
            "Country"        => $this->post("Country"),
            "Zipcode"        => $this->post("Zipcode"),
            "RecordStatusId" => $this->post("RecordStatusId"),
            "CreatedOn"      => $this->post("CreatedOn"),
            "CreatedBy"      => $this->post("CreatedBy"),
            "UpdatedOn"      => $this->post("UpdatedOn"),
            "UpdatedBy"      => $this->post("UpdatedBy")
        );

        if ($id != "") {
            $this->db->where('Id', $id);
            $this->db->update('Address', $data);
        } else {
            $this->db->insert('Address', $data);
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error() : "";

        $this->response(array(
            "success" => $res,
            "error" => $err
        ), 200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  DeleteAddress
     * </Name:>
     * <Parameters:>
     *  Id
     * </Parameters:>
     */
    function DeleteAddress_post()
    {
        $id = $this->post("Id");

        $err = "";
        if ($id != "") {
            $this->db->where("Id", $id);
            $this->db->delete("Address");
        } else {
            $err = "PRIMARY KEY cannot null";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;

        $this->response(array(
            "success" => $res,
            "error" => $err
        ), 200);
    }
}