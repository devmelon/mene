<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class User extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    /**
     * <Method:>
     *  GET
     * </Method:>
     * <Name:>
     *  GetUser
     * </Name:>
     * <Parameters:>
     *  Id
     *  TenantId
     * </Parameters:>
     */
    function GetUser_get()
    {
        $id        = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM User WHERE TenantId = $tenant_id AND RecordStatusId = 0";
        $sql .= $id != "" ? " AND Id = $id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  UpdateUser
     * </Name:>
     * <Parameters:>
     *  Id
     *  TenantId
     *  User1
     *  User2
     *  PhoneNumber
     *  City
     *  Province
     *  Country
     *  Zipcode
     *  RecordStatusId
     *  CreatedOn
     *  CreatedBy
     *  UpdatedOn
     *  UpdatedBy
     * </Parameters:>
     */
    function UpdateUser_post()
    {
        $id = $this->post("Id");
        $data = array(
            "RoleId"         => $this->post("RoleId"),
            "FirstName"      => $this->post("FirstName"),
            "LastName"       => $this->post("LastName"),
            "EmailAddress"   => $this->post("EmailAddress"),
            "Password"       => $this->post("Password"),
            "RecordStatusId" => 0
        );

        if (!$this->helper->is_new($id)) {
            $this->db->where('Id', $id);
            $this->db->update('User', $data);
        } else {
            $this->db->insert('User', $data);
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error()->message : $err;
        $suc = $res ? "Ubah User Berhasil" : "";

        $this->response(array(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        ), 200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  DeleteUser
     * </Name:>
     * <Parameters:>
     *  Id
     * </Parameters:>
     */
    function DeleteUser_post()
    {
        $id = $this->post("Id");

        $err = "";
        if ($id != "") {
            $this->db->where("Id", $id);
            $this->db->update("User", array("RecordStatusId" => 2));
        } else {
            $err = "PRIMARY KEY cannot null";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;

        $this->response(array(
            "success" => $res,
            "error" => $err
        ), 200);
    }
}