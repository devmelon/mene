<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Person extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    function DummyDataPerson_get(){
        $tenant_id = $this->get("TenantId");
        $user_id = $this->get("UserId");
        $total = (int)$this->get("Total");
        $bloodType = ["A", "B", "AB", "O"];
        $genderId = ["1", "2"];
        $name = ["Abigail","Alexandra","Alison","Amanda","Amelia","Amy","Andrea","Angela","Anna","Anne","Audrey","Ava","Bella","Bernadette","Carol","Caroline","Carolyn","Chloe","Claire","Deirdre","Diana","Diane","Donna","Dorothy","Elizabeth","Ella","Emily","Emma","Faith","Felicity","Fiona","Gabrielle","Grace","Hannah","Heather","Irene","Jan","Jane","Jasmine","Jennifer","Jessica","Joan","Joanne"];
        $dates = ["2016-01-01", "2013-01-01", "2010-01-01", "2007-01-01", "2002-01-01", "1998-01-01", "1992-01-01", "1989-01-01", "1985-01-01"];

        for($i = 0; $i < $total; $i++){
            $birthDateRand  = rand(0,8);
            $bloodTypeRand  = rand(0,3);
            $genderRand     = rand(0,1);
            $firstRand     = rand(0,42);
            $lastRand     = rand(0,42);

            $data = array(
                "TenantId"         => $tenant_id,
                "FirstName"        => $name[$firstRand],
                "LastName"         => $name[$lastRand],
                "GenderId"         => $genderId[$genderRand],
                "BirthPlace"       => "Jakarta",
                "BirthDate"        => $dates[$birthDateRand],
                "BloodType"        => $bloodType[$bloodTypeRand],
                "RecordStatusId"   => 0,
                "CreatedBy"        => $user_id,
                "CreatedOn"        => date("Y-m-d h:i:s")
            );

            $this->db->insert('Person', $data);
        }
    }

    function GetPersons_get()
    {
        $id        = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT p.*, CONCAT(p.FirstName, ' ', p.LastName) FullName, g.Name GenderName FROM Person p inner join Gender g on p.GenderId = g.Id WHERE TenantId = $tenant_id AND RecordStatusId = 0";
        $sql .= $id != "" ? " AND p.Id = $id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    public function PrepareUpdatePerson_get(){
        $tenant_id = $this->get("TenantId");
        $data = [];
        $res = false;

        $sql = "SELECT * FROM MaritalStatus";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $res = true;
            $data["MaritalStatus"] = $query->result();
        }

        $sql = "SELECT * FROM Gender";
        $query = $this->db->query($sql);
        if ($res && $query->num_rows() > 0) {
            $data["Gender"] = $query->result();
        }

        $sql_note_type = "SELECT * FROM NoteType WHERE TenantId = $tenant_id AND RecordStatusId = 0";
        $q_note_type = $this->db->query($sql_note_type);

        if ($q_note_type->num_rows() > 0) {
            $res = true;
            $data["NoteType"] = $q_note_type->result();
        }

        $this->response(
            $this->helper->response(
                $res,
                $data,
                "",
                ""
            ),
            200
        );
    }

    public function UpdatePerson_post()
    {
        $id = $this->post("Id");
        $tenant_id = $this->post("TenantId");
        $address_id = $this->post("AddressId");
        $user_id = $this->post("UserId");

        $data = array(
            "TenantId"         => $tenant_id,
            "FirstName"        => $this->post("FirstName"),
            "LastName"         => $this->post("LastName"),
            "GenderId"         => $this->post("GenderId"),
            "BirthPlace"       => $this->post("BirthPlace"),
            "BirthDate"        => $this->post("BirthDate"),
            "BloodType"        => $this->post("BloodType"),
            "EmailAddress"     => $this->post("EmailAddress"),
            "PhoneNumber"      => $this->post("PhoneNumber"),
            "MaritalStatusId"  => $this->post("MaritalStatusId"),
            "RecordStatusId"   => 0
        );

        $address = array(
            "TenantId"       => $tenant_id,
            "Address1"       => $this->post("Address1"),
            "Address2"       => $this->post("Address2"),
            "PhoneNumber"    => $this->post("PhoneNumber"),
            "CityId"         => $this->post("CityId"),
            "ProvinceId"     => $this->post("ProvinceId"),
            "CountryId"      => $this->post("CountryId"),
            "Zipcode"        => $this->post("Zipcode"),
            "RecordStatusId" => 0
        );

        $note = array(
            "TenantId"       => $tenant_id,
            "NoteTypeId"     => $this->post("NoteTypeId"),
            "Description"    => $this->post("Description"),
            "RecordStatusId" => 0
        );

        if (!$this->helper->is_new($id)) {
            // $address["UpdatedOn"] = $this->helper->get_now();
            // $address["UpdatedBy"] = $this->post("UserId");

            // $this->db->where("Id", $address_id);
            // $this->db->update('Address', $data["address"]);

            $data["UpdatedBy"] = $this->post("UserId");
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('Person', $data);
        } else {
            // $address["CreatedOn"] = $this->helper->get_now();
            // $address["CreatedBy"] = $this->post("UserId");

            // $this->db->insert('Address', $data["address"]);
            // $address_id = $this->db->insert_id();

            // $data["AddressId"] = $address_id;
            $data["CreatedBy"] = $this->post("UserId");
            $data["CreatedOn"] = date("Y-m-d h:i:s");

            $this->db->insert('Person', $data);
            // var_dump($this->db->last_query());die;
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error()->message : $err;
        $suc = $res ? "Data berhasil disimpan" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            )
        , 200);
    }

    function DeletePerson_post()
    {
        $tenant_id = $this->post("TenantId");
        $id = $this->post("Id");
        $userId = $this->post("UserId");

        $err = "";
        if ($id != "") {
            $data["RecordStatusId"] = 2;
            $data["UpdatedBy"] = $userId;
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('Person', $data);
        } else {
            $err = "Parameter tidak valid";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                "Data berhasil dihapus"
            )
        , 200);
    }

}
