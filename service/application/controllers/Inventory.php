<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Inventory extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('note_model', 'nm');
    }

    //region Inventory
    function GetInventory_get()
    {
        $id        = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM Inventory i LEFT JOIN InventoryItem ii ON ii.Id = i.ItemId LEFT JOIN Note n ON n.Id = i.NoteId WHERE i.TenantId = '$tenant_id' AND i.RecordStatusId = 0";
        $sql .= $id != "" ? " AND i.Id = $id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    public function PrepareUpdateInventory_get()
    {
        $tenant_id = $this->get("TenantId");
        $data = [];
        $res = false;

        $sql = "SELECT * FROM InventoryItem WHERE TenantId = $tenant_id AND RecordStatusId = 0";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $res = true;
            $data["InventoryItem"] = $query->result();
        }
        
        $sql_note_type = "SELECT * FROM NoteType WHERE TenantId = $tenant_id AND RecordStatusId = 0";
        $q_note_type = $this->db->query($sql_note_type);

        if($q_note_type->num_rows() > 0){
            $res = true;
            $data["NoteType"] = $q_note_type->result();
        }

        $this->response(
            $this->helper->response(
                $res,
                $data,
                "",
                ""
            ),
            200
        );
    }

    public function UpdateInventory_post()
    {
        $id      = $this->post("Id");
        $user_id = $this->post("UserId");
        // $note_id = $this->post("NoteId");

        $data = array(
            "TenantId"       => $this->post("TenantId"),
            "ItemId"         => $this->post("ItemId"),
            "Total"          => $this->post("Total"),
            "RecordStatusId" => 0
        );

        // $note = array(
        //     "TenantId"       => $this->post("TenantId"),
        //     "NoteTypeId"     => $this->post("NoteTypeId"),
        //     "Description"    => $this->post("Description"),
        //     "RecordStatusId" => 0
        // );

        $err = "";
        $now = date("Y-m-d H:i:s");
        if (!$this->helper->is_new($id)) {
            $data["UpdatedOn"] = $now;
            $data["UpdatedBy"] = $user_id;

            // $note["UpdatedOn"] = $now;
            // $note["UpdatedBy"] = $user_id;
            // $this->nm->update_note($note_id, $note);

            // $data["NoteId"] = $note_id;
            $this->db->where('Id', $id);
            $this->db->update('Inventory', $data);
        } else {
            $data["CreatedOn"] = $now;
            $data["CreatedBy"] = $user_id;

            // $note["CreatedOn"] = $now;
            // $note["CreatedBy"] = $user_id;
            // $note_id = $this->nm->insert_note($note);
            // print_r($note);die;

            // $data["NoteId"] = $note_id;
            $this->db->insert('Inventory', $data);
            $inventory_item_id = $this->db->insert_id();
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error()->message : "";
        $suc = $res ? "Data Berhasil Disimpan!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }
    //endregion Inventory

    //region InventoryCategory
    function GetInventoryCategory_get()
    {
        $id        = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM InventoryCategory WHERE TenantId = '$tenant_id' AND RecordStatusId = 0";
        $sql .= $id != "" ? " AND Id = $id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    public function UpdateInventoryCategory_post()
    {
        $id      = $this->post("Id");
        $user_id = $this->post("UserId");

        $data = array(
            "TenantId"       => $this->post("TenantId"),
            "Name"           => $this->post("Name"),
            "RecordStatusId" => 0
        );

        $err = "";
        if (!$this->helper->is_new($id)) {
            $data["UpdatedOn"] = date("Y-m-d H:i:s");
            $data["UpdatedBy"] = $user_id;

            $this->db->where('Id', $tenant_id);
            $this->db->update('InventoryCategory', $data);
        } else {
            $data["CreatedOn"] = date("Y-m-d H:i:s");
            $data["CreatedBy"] = $user_id;

            $this->db->insert('InventoryCategory', $data);
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error()->message : "";
        $suc = $res ? "Data Berhasil Disimpan!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            )
        , 200);
    }

    function DeleteInventoryCategory_post()
    {
        $id = $this->post("Id");

        $err = "";
        if ($id != "") {
            $this->db->where("Id", $id);
            $this->db->update("InventoryCategory", array(
                "RecordStatusId" => 2
            ));
        } else {
            $err = "PRIMARY KEY cannot null";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;
        $suc = $res ? "Hapus Data Berhasil!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            )
        200);
    }
    //endregion InventoryCategory

    //region InventoryItem
    function GetInventoryItem_get()
    {
        $id = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM InventoryItem WHERE TenantId = '$tenant_id' AND RecordStatusId = 0";
        $sql .= $id != "" ? " AND Id = $id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    public function PrepareUpdateInventoryItem_get()
    {
        $tenant_id = $this->post("TenantId");
        $data = [];
        $res = false;

        $sql = "SELECT * FROM InventoryCategory WHERE TenantId = $tenant_id AND RecordStatus = 0";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $res = true;
            $data["InventoryCategory"] = $query->result();
        }

        $this->response(
            $this->helper->response(
                $res,
                $data,
                "",
                ""
            ),
            200
        );
    }

    public function UpdateInventoryItem_post()
    {
        $id                   = $this->post("Id");
        $user_id              = $this->post("UserId");
        $list_invent_category = $this->post("ListInventoryCategory");

        $data = array(
            "TenantId" => $this->post("TenantId"),
            "Name" => $this->post("Name"),
            "RecordStatusId" => 0
        );

        $err = "";
        if (!$this->helper->is_new($id)) {
            $data["UpdatedOn"] = date("Y-m-d H:i:s");
            $data["UpdatedBy"] = $user_id;

            $this->db->where('Id', $id);
            $this->db->update('InventoryItem', $data);
        } else {
            $data["CreatedOn"] = date("Y-m-d H:i:s");
            $data["CreatedBy"] = $user_id;

            $this->db->insert('InventoryItem', $data);
            $inventory_item_id = $this->db->insert_id();

            foreach($list_invent_category as $cat){
                $item_category = array(
                    "InventoryItemId"     => $inventory_item_id,
                    "InventoryCategoryId" => $cat
                );

                $this->db->insert("InventoryItemCategory", $item_category);
            }
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error()->message : "";
        $suc = $res ? "Data Berhasil Disimpan!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            )
        , 200);
    }

    function DeleteInventoryItem_post()
    {
        $tenant_id = $this->post("TenantId");
        $id = $this->post("Id");
        $userId = $this->post("UserId");

        $err = "";
        if ($id != "") {
            $data["RecordStatusId"] = 2;
            $data["UpdatedBy"] = $userId;
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('InventoryItem', $data);
        } else {
            $err = "PRIMARY KEY cannot null";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;
        $suc = $res ? "Hapus Data Berhasil!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            )
        , 200);
    }
    //endregion InventoryCategory

    
}