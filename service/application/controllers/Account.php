<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Account extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    function CheckLogin_post()
    {
        $emailAddress   = $this->post("EmailAddress");
        $password       = md5($this->post("Password"));

        $sql = "SELECT * FROM User WHERE EmailAddress = '$emailAddress' and Password = '$password' and RecordStatusId = 0";

        $data = "";
        if ($emailAddress == "" || $password == "") {
            $err = "Login invalid";
        } else {
            $query = $this->db->query($sql);
            $data = $query->row();
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }
}