<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Tenant extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('Tenant_model', 'tm');
    }

    //region Tenant
    /**
     * <Mehotd:>
     *  GET
     * </Mehotd:>
     * <Name:>
     *  GetTenant
     * </Name:>
     * <Parameters:>
     *  Id
     * </Parameters:>
     */
    function GetTenant_get()
    {
        $id = $this->get("Id");

        $sql = "SELECT * FROM Tenant t LEFT JOIN Address a ON a.Id = t.AddressId WHERE t.Id = '$id' AND t.RecordStatusId = 0";

        $data = "";
        if($id == ""){
            $err = "Parameter Id harus diisi!";
        }else{
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            )
        , 200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  InsertTenant
     * </Name:>
     * <Parameters:>
     *  TenantName
     *  TenantTypeId
     *  FirstName
     *  LastName
     *  EmailAddress
     *  Password
     * </Parameters:>
     */
    function InsertTenant_post()
    {
        $data = array(
            "tenant" => array(
                "Name"           => $this->post("TenantName"),
                "TenantTypeId"   => $this->post("TenantTypeId"),
                "RecordStatusId" => 0
            ),
            "user" => array(
                "FirstName"      => $this->post("FirstName"),
                "LastName"       => $this->post("LastName"),
                "EmailAddress"   => $this->post("EmailAddress"),
                "Password"       => $this->post("Password"),
                "RecordStatusId" => 0
            )
        );

        $data["tenant"]["CreatedOn"] = "NOW()";
        $this->db->insert("Tenant", $data["tenant"]);

        $tenant_id = $this->db->insert_id();

        $sql_role = "
            INSERT INTO Role (TenantId, Name, RecordStatusId, CreatedOn)
            SELECT '$tenant_id', Name, RecordStatusId, NOW() FROM Role WHERE TenantId = 1 AND RecordStatusId = 0
            ";
        $this->db->query($sql_role);

        $sql_user = "
            INSERT INTO User 
            (TenantId, RoleId, FirstName, LastName, EmailAddress, Password, RecordStatusId, CreatedOn)
            VALUES 
            (
                '$tenant_id', 
                (SELECT Id FROM Role WHERE TenantId = $tenant_id AND LOWER(Name) = 'administrator' AND RecordStatusId = 0 LIMIT 0,1),
                '" . $data["user"]["FirstName"] . "',
                '" . $data["user"]["LastName"] . "',
                '" . $data["user"]["EmailAddress"] . "',
                '" . md5($data["user"]["Password"]) . "',
                '" . $data["user"]["RecordStatusId"] . "',
                NOW() 
            )
        ";
        $this->db->query($sql_user);

        $user_id = $this->db->insert_id();

        $created_by = array("CreatedBy" => $user_id);
        $this->db->update("Tenant", $created_by);
        $this->db->update("Role", $created_by);
        $this->db->update("User", $created_by);

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error()->message : "";
        $suc = $res ? "Registrasi Berhasil!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  UpdateTenant
     * </Name:>
     * <Parameters:>
     *  UserId
     *  TenantId
     *  TenantName
     *  Address1
     *  Address2
     *  PhoneNumber
     *  City
     *  Province
     *  Country
     *  Zipcode
     * </Parameters:>
     */
    public function UpdateTenant_post()
    {
        $tenant_id = $this->post("TenantId");
        $address_id = $this->post("AddressId");

        $data = array(
            "UserId" => $this->post("UserId"),

            "tenant" => array(
                "TenantTypeId"   => $this->post("TenantTypeId"),
                "Name"           => $this->post("TenantName"),
                "RecordStatusId" => 0
            ),
            "address" => array(
                "TenantId"       => $this->post("TenantId"),
                "Address1"       => $this->post("Address1"),
                "Address2"       => $this->post("Address2"),
                "PhoneNumber"    => $this->post("PhoneNumber"),
                "CityId"         => $this->post("CityId"),
                "ProvinceId"     => $this->post("ProvinceId"),
                "CountryId"      => $this->post("CountryId"),
                "Zipcode"        => $this->post("Zipcode"),
                "RecordStatusId" => 0
            )
        );

        $err = "";
        if ($tenant_id != "") {
            if($this->helper->is_new($address_id)){
                $data["address"]["CreatedOn"] = $this->helper->get_now();
                $data["address"]["CreatedBy"] = $data["UserId"];

                $this->db->insert('Address', $data["address"]);
                $address_id = $this->db->insert_id();
            }else{
                $data["address"]["UpdatedOn"] = $this->helper->get_now();
                $data["address"]["UpdatedBy"] = $data["UserId"];

                $this->db->where('Id', $address_id);
                $this->db->update('Address', $data["address"]);
            }

            $data["tenant"]["AddressId"] = $address_id;
            $data["tenant"]["UpdatedOn"] = $this->helper->get_now();
            $data["tenant"]["UpdatedBy"] = $data["UserId"];

            $this->db->where('Id', $tenant_id);
            $this->db->update('Tenant', $data["tenant"]);
        }else{
            $err = "Please Provide TenantId";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error()->message : "";
        $suc = $res ? "Data Berhasil Disimpan!" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  DeleteTenant
     * </Name:>
     * <Parameters:>
     *  Id
     * </Parameters:>
     */
    function DeleteTenant_post()
    {
        $id = $this->post("Id");

        $err = "";
        if($id != ""){
            $this->db->where("Id", $id);
            $this->db->update("Tenant", array(
                "RecordStatusId" => 2
            ));
        }else{
            $err = "PRIMARY KEY cannot null";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;
        $suc = $res ? "Hapus Tenant Berhasil!" : "";
   
        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }
    //endregion Tenant

    //region TenantFeature
    /**
     * <Method:>
     *  GET
     * </Method:>
     * <Name:>
     *  GetTenantFeature
     * </Name:>
     * <Parameters:>
     *  TenantId
     * </Parameters:>
     */
    function GetTenantFeature_get()
    {
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM TenantFeature WHERE TenantId = '$tenant_id'";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  UpdateTenantFeature
     * </Name:>
     * <Parameters:>
     *  Id
     *  UserId
     *  TenantId
     *  FeatureId
     *  RecordStatusId
     *  CreatedOn
     *  CreatedBy
     *  UpdatedOn
     *  UpdatedBy
     * </Parameters:>
     */
    function UpdateTenantFeature_post()
    {
        $id = $this->post("Id");
        $data = array(
            "UserId"         => $this->post("UserId"),
            "TenantId"       => $this->post("TenantId"),
            "FeatureId"      => $this->post("FeatureId"),
            "RecordStatusId" => $this->post("RecordStatusId")
        );

        // if ($id != "") {
        if(!$this->helper->is_new($id)) {
          
            $data["UpdatedOn"] = "NOW()";
            $data["UpdatedBy"] = $data["UserId"];
          
            // TODO Address

            $this->db->where('Id', $id);
            $this->db->update('TenantFeature', $data);
        } else {
            $data["CreatedOn"] = "NOW()";
            $data["CreatedBy"] = $data["UserId"];

            // TODO Address
            
            $this->db->insert('TenantFeature', $data);
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res ? $this->db->error()->message : "";
        $suc = $res ? "Ubah Fitur Tenan Berhasil" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }

    /**
     * <Method:>
     *  POST
     * </Method:>
     * <Name:>
     *  DeleteTenantFeature
     * </Name:>
     * <Parameters:>
     *  Id
     * </Parameters:>
     */
    function DeleteTenantFeature_post()
    {
        $id = $this->post("Id");

        $err = "";
        if ($id != "") {
            $this->db->where("Id", $id);
            $this->db->update("TenantFeature", array(
                "RecordStatusId" => 2
            ));
        } else {
            $err = "PRIMARY KEY cannot null";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error()->message : $err;
        $suc = $res ? "Hapus Fitur Tenan Berhasil" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            )
        , 200);
    }
    //endregion TenantFeature

    //region TenantType
    /**
     * <Mehotd:>
     *  GET
     * </Mehotd:>
     * <Name:>
     *  GetTenantTypes
     * </Name:>
     */
    function GetTenantTypes_get()
    {
        $sql = "SELECT * FROM TenantType";

        $data = "";
     
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
     
        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }
    //endregion TenantType
}
