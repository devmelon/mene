<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Event extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    function GetEvents_get()
    {
        $id        = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM Event WHERE TenantId = $tenant_id AND RecordStatusId = 0";
        $sql .= $id != "" ? " AND Id = $id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    public function UpdateEvent_post()
    {
        $id = $this->post("Id");
        $tenant_id = $this->post("TenantId");
        $data = array(
            "TenantId"          => $tenant_id,
            "LedgerId"          => $this->post("LedgerId"),
            "Name"              => $this->post("Name"),
            "Description"       => $this->post("Description"),
            "StartEvent"        => $this->post("StartEvent"),
            "EndEvent"          => $this->post("EndEvent"),
            "Location"          => $this->post("Location"),
            "Speaker"           => $this->post("Speaker"),
            "Topic"             => $this->post("Topic"),
            "TotalAttendants"   => $this->post("TotalAttendants"),
            "Evaluation"        => $this->post("Evaluation"),
            "RecordStatusId"    => 0
        );

        if (!$this->helper->is_new($id)) {
            $data["UpdatedBy"] = $this->post("UserId");
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('Event', $data);
        } else {
            $dataLedger = array(
                "TenantId"          => $tenant_id,
                "Title"             => $this->post("Name"),
                "RecordStatusId"    => 0,
                "CreatedBy"         => $this->post("UserId"),
                "CreatedOn"         => date("Y-m-d h:i:s"),
            );

            $this->db->insert('Ledger', $dataLedger);
            $ledger_id = $this->db->insert_id();

            // print_r($this->db->last_query());die;

            $data["LedgerId"] = $ledger_id;
            $data["CreatedBy"] = $this->post("UserId");
            $data["CreatedOn"] = date("Y-m-d h:i:s");

            $this->db->insert('Event', $data);
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error()->message : $err;
        $suc = $res ? "Data berhasil disimpan" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
         200);
    }

    function DeleteEvent_post()
    {
        $tenant_id = $this->post("TenantId");
        $id = $this->post("Id");
        $userId = $this->post("UserId");

        $err = "";
        if ($id != "") {
            $data["RecordStatusId"] = 2;
            $data["UpdatedBy"] = $userId;
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('Event', $data);
        } else {
            $err = "Parameter tidak valid";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                "Data berhasil dihapus"
            ),
         200);
    }
}
