<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Report extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    function ReportDashboard_get(){
        $tenant_id = $this->get("TenantId");
        $data = [];

        $sql = "
            select g.Name as Gender, count(p.Id) as Total from Person p
            inner join Gender g on g.Id = p.GenderId
            where p.TenantId = $tenant_id and p.RecordStatusId = 0
            group by p.GenderId;
        ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data["Gender"] = $query->result();
        }

        $sql = "
            select IF(p.BloodType <> '', UPPER(p.BloodType), 'Kosong') as BloodType, count(p.Id) as Total from Person p
            where TenantId = $tenant_id and p.RecordStatusId = 0
            group by UPPER(p.BloodType)
            order by p.BloodType;
        ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data["BloodType"] = $query->result();
        }

        $sql = "
            select concat(15 * floor(age/15), '-', 15 * floor(age/15) + 14) as AgeRange, SUM(Total) Total from (
            select YEAR(NOW()) - YEAR(BirthDate) as Age, count(1) as Total from Person p
            where TenantId = $tenant_id and p.RecordStatusId = 0
            group by 1 order by 1) a group by 1 order by 1;
        ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data["Age"] = $query->result();
        }

        $sql = "
            select Name, TotalAttendance FROM Event WHERE TenantId = $tenant_id  
            AND (TotalAttendance <> null OR TotalAttendance <> 0)
        ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data["Attendance"] = $query->result();
        }

        $this->response(
            $this->helper->response(
                true,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    function ReportEventAttendance_get()
    {
        $tenant_id = $this->get("TenantId");

        $sql = "
            SELECT Name,  TotalAttendance FROM Event WHERE TenantId = $tenant_id  
            AND (TotalAttendance <> null OR TotalAttendance <> 0)
        ";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    function ReportPersonByGender_get()
    {
        $tenant_id = $this->get("TenantId");

        $sql = "
            select g.Name as Gender, count(p.Id) as Count from Person p
            left join Gender g on g.Id = p.GenderId
            where TenantId = $tenant_id
            group by GenderId;
        ";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    function ReportPersonByBloodType_get()
    {
        $tenant_id = $this->get("TenantId");

        $sql = "
            select UPPER(p.BloodType), count(p.Id) from Person p
            where TenantId = $tenant_id
            group by UPPER(p.BloodType);
        ";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }
}