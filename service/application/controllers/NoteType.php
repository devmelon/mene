<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class NoteType extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    function GetNoteTypes_get()
    {
        $id         = $this->get("Id");
        $tenant_id  = $this->get("TenantId");
        $feature_id = $this->get("FeatureId");

        $sql = "SELECT n.*, f.Name as FeatureName FROM NoteType n inner join Feature f on n.FeatureId = f.Id WHERE TenantId = $tenant_id and RecordStatusId = 0";
        $sql .= $id != "" ? " AND Id = $id" : "";
        $sql .= $feature_id != "" ? " AND FeatureId = $feature_id" : "";

        $data = "";
        if ($tenant_id == "") {
            $err = "Parameter TenantId harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    function UpdateNoteType_post()
    {
        $id = $this->post("Id");
        $tenant_id = $this->post("TenantId");

        $data = array(
            "TenantId"          => $tenant_id,
            "FeatureId"         => $this->post("FeatureId"),
            "Name"              => $this->post("Name"),
            "RecordStatusId"    => 0
        );

        if (!$this->helper->is_new($id)) {
            $data["UpdatedBy"] = $this->post("UserId");
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('NoteType', $data);
        } else {
            $data["CreatedBy"] = $this->post("UserId");
            $data["CreatedOn"] = date("Y-m-d h:i:s");

            $this->db->insert('NoteType', $data);
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error()->message : $err;
        $suc = $res ? "Data berhasil disimpan" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }

    function DeleteNoteType_post()
    {
        $tenant_id = $this->post("TenantId");
        $id = $this->post("Id");
        $userId = $this->post("UserId");

        $err = "";
        if ($id != "") {
            $data["RecordStatusId"] = 2;
            $data["UpdatedBy"] = $userId;
            $data["UpdatedOn"] = date("Y-m-d h:i:s");

            $whereClause = array(
                "Id" => $id,
                "TenantId" => $tenant_id
            );

            $this->db->where($whereClause);
            $this->db->update('NoteType', $data);
        } else {
            $err = "Parameter tidak valid";
        }

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error() : $err;

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                "Data berhasil dihapus"
            ),
        200);
    }
}