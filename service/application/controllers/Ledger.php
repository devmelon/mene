<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
require(APPPATH . 'libraries/REST_Controller.php');

class Ledger extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    function GetLedger_get()
    {
        $id        = $this->get("Id");
        $tenant_id = $this->get("TenantId");

        $sql = "SELECT * FROM Ledger WHERE TenantId = $tenant_id AND Id = $id AND RecordStatusId = 0";

        $data = "";
        if ($tenant_id == "" || $id == "") {
            $err = "Parameter harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }

    function GetLedgerTransactions_get()
    {
        $ledgerId       = $this->get("LedgerId");
        $tenant_id      = $this->get("TenantId");

        $sql = "SELECT * FROM LedgerTransaction WHERE TenantId = $tenant_id AND LedgerId = $ledgerId AND RecordStatusId = 0";

        $data = "";
        if ($tenant_id == "" || $ledgerId == "") {
            $err = "Parameter harus diisi!";
        } else {
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
            }
        }

        $err = $err == "" ? $this->db->error()->message : $err;
        $res = $err != "" ? false : true;
        $suc = $res ? "you got the data!" : "";

        $this->response(
            $this->helper->response(
                $res,
                $data,
                $err,
                $suc
            ),
            200
        );
    }    

    public function InsertLedgerTransaction_post()
    {
        $tenant_id = $this->post("TenantId");
        $data = array(
            "TenantId"        => $tenant_id,
            "LedgerId"        => $this->post("LedgerId"),
            "TotalCash"       => $this->post("TotalCash"),
            "RecordStatusId"  => 0,
            "CreatedBy"       => $this->post("UserId"),
            "CreatedOn"       => date("Y-m-d h:i:s")
        );

        $this->db->insert('LedgerTransaction', $data);
        // var_dump($this->db->last_query());die;

        $res = $this->db->affected_rows() > 0;
        $err = !$res && $err == "" ? $this->db->error()->message : $err;
        $suc = $res ? "Data berhasil disimpan" : "";

        $this->response(
            $this->helper->response(
                $res,
                "",
                $err,
                $suc
            ),
        200);
    }
}
