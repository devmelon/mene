<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Note_model extends CI_Model
{
    function Note_model()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert_note($data)
    {
        $this->db->insert("Note", $data);
        return $this->db->insert_id();
    }

    public function update_note($id, $data)
    {
        $this->db->where("Id", $id);
        $this->db->update('Note', $data);
    }
}