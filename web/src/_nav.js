export default {
  items: [
    {
      name: 'Beranda',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      title: true,
      name: 'Data Acara',
      class: '',
    },
    {
      name: 'Acara',
      url: '/event',
      icon: 'fa fa-calendar',
    },
    {
      title: true,
      name: 'Data Jemaat',
      class: '',
    },
    {
      name: 'Jemaat',
      url: '/person',
      icon: 'fa fa-users',
    },
    {
      title: true,
      name: 'Data Inventaris',
      class: '',
    },
    {
      name: 'Inventaris',
      url: '/inventory',
      icon: 'fa fa-diamond',
    },
    {
      title: true,
      name: 'Laporan',
      class: '',
    },
    {
      name: 'Tren Acara',
      url: '/report/eventtren',
      icon: 'fa fa-book',
    },
    {
      name: 'Demografik Jemaat',
      url: '/report/peopletren',
      icon: 'fa fa-book',
    },
    {
      name: 'Finansial Acara',
      url: '/report/eventfinancial',
      icon: 'fa fa-book',
    },
    {
      title: true,
      name: 'Setting',
      class: '',
    },
    {
      name: 'Catatan',
      url: '/setting/notes',
      icon: 'fa fa-gear',
    },
  ]
}
