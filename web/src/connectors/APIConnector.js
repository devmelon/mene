﻿// library
import axios from 'axios'
import store from '../store'

// axios instance
var oAxios = axios.create({
  baseURL: "http://192.168.8.107:8123/mene/mene/service/",
  headers: {
    'Access-Control-Allow-Origin': '*'
  }
});

oAxios.interceptors.response.use(response => {
  return response;
}, error => {
  if (!error.response) {
    error.response = {
      data: { error_description: error.message }
    }
  }
  return Promise.reject(error);
});

export default oAxios;