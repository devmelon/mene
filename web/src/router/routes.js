// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// Pages  
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

const Event = () => import('@/views/Event')
const EventAdd = () => import('@/views/event/Add')
const EventEdit = () => import('@/views/event/Edit')
const EventDelete = () => import('@/views/event/Delete')
const EventLedger = () => import('@/views/event/Ledger')
const EventLedgerAdd = () => import('@/views/event/ledger/Add')

const Person = () => import('@/views/Person')
const PersonAdd = () => import('@/views/person/Add')
const PersonEdit = () => import('@/views/person/Edit')
const PersonDelete = () => import('@/views/person/Delete')

const Inventory = () => import('@/views/Inventory')
const InventoryAdd = () => import('@/views/inventory/Add')
const InventoryEdit = () => import('@/views/inventory/Edit')
const InventoryDelete = () => import('@/views/inventory/Delete')

const InventoryDetail = () => import('@/views/InventoryDetail')
const InventoryDetailAdd = () => import('@/views/inventoryDetail/Add')
const InventoryDetailEdit = () => import('@/views/inventoryDetail/Edit')

const EventTren = () => import('@/views/report/EventTren')
const EventFinancial = () => import('@/views/report/EventFinancial')
const PeopleTren = () => import('@/views/report/PeopleTren')

export default [
  {
    path: '/',
    redirect: '/dashboard',
    name: 'Home',
    component: DefaultContainer,
    children: [
      { 
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      // event
      {
        path: 'event',
        name: 'Event',
        component: Event,
      },
      {
        path: 'event/add',
        name: 'Add Event',
        component: EventAdd
      },
      {
        path: 'event/edit/:id',
        name: 'Edit Event',
        component: EventEdit
      },
      {
        path: 'event/delete/:id',
        name: 'Delete Event',
        component: EventDelete
      },
      {
        path: 'event/ledger/:id',
        name: 'Ledger Event',
        component: EventLedger
      },
      {
        path: 'event/ledger/:id/add',
        name: 'Add Ledger Event',
        component: EventLedgerAdd
      },

      // person
      {
        path: 'person',
        name: 'Person',
        component: Person,
      },
      {
        path: 'person/add',
        name: 'Add Person',
        component: PersonAdd
      },
      {
        path: 'person/edit/:id',
        name: 'Edit Person',
        component: PersonEdit
      },
      {
        path: 'person/delete/:id',
        name: 'Delete Person',
        component: PersonDelete
      },
      // inventory
      {
        path: 'inventory',
        name: 'Inventory',
        component: Inventory,
      },
      {
        path: 'inventory/add',
        name: 'Add Inventory',
        component: InventoryAdd
      },
      {
        path: 'inventory/edit/:id',
        name: 'Edit Inventory',
        component: InventoryEdit
      },
      {
        path: 'inventory/delete/:id',
        name: 'Delete Inventory',
        component: InventoryDelete
      },
      // inventory detail
      {
        path: 'inventoryDetail/:itemId',
        name: 'InventoryDetail',
        component: InventoryDetail,
      },
      {
        path: 'inventoryDetail/add/:itemId',
        name: 'Add InventoryDetail',
        component: InventoryDetailAdd
      },
      {
        path: 'inventoryDetail/edit/:itemId/:id',
        name: 'Edit InventoryDetail',
        component: InventoryDetailEdit
      },
      // laporan
      {
        path: 'report/peopleTren',
        name: 'Report People Tren',
        component: PeopleTren
      },
      {
        path: 'report/eventTren',
        name: 'Report Event Tren',
        component: EventTren
      },
      {
        path: 'report/eventFinancial',
        name: 'Report Event Financial',
        component: EventFinancial
      },
    ],
    meta: {
      auth: true
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
]
