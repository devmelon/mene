import Vue from 'vue'
import Router from 'vue-router'

// module
import routes from './routes'
import Middleware from './middleware'

Vue.use(Router)

const router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: routes
})

Middleware.requierdLogin(router)

export default router
