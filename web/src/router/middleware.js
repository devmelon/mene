﻿import store from '../store'

export default {

  requierdLogin: router => {
    router.beforeEach((to, from, next) => {

      // check auth requirement
      let reqAuth

      if (to.auth) {
        reqAuth = to.auth
      } else {
        let authRoutes = to.matched.filter(function (route) {
          return route.meta.hasOwnProperty('auth');
        });

        // matches the nested route, the last one in the list
        if (authRoutes.length) {
          reqAuth = authRoutes[authRoutes.length - 1].meta.auth;
        } else {
          reqAuth = false
        }
      }

      // check auth status
      let isAuth = store.state.auth.isAuthenticated

      // sync with cookies
      if (!isAuth) {
        store.dispatch('auth/syncLogin')
        isAuth = store.state.auth.isAuthenticated
      }

      console.log(reqAuth, isAuth);

      if (reqAuth && !isAuth) {
        next('/Login');
      } else {
        next();
      }
    });
  }
  
};