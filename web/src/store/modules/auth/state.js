﻿export default {
  isAuthenticated: false,
  user: {
    firstname: null,
    userId: null,
    tenantId: null,
  },
}
