﻿// library
import oAxios from '@/connectors/APIConnector'
import Cookies from 'js-cookie'
import { debug } from 'util';

export default {
  register: async (context, params) => {
    await oAxios.post('Tenant/InsertTenant', params)
      .then(response => {
        if (response.data.success) {
          context.dispatch('app/setMessageSuccess', response.data.message.success, { root: true })
        } else {
          context.dispatch('app/setMessageError', response.data.message.error, { root: true })
        }
      }).catch(error => {
        context.dispatch('app/setMessageError', error.response.data.error_description, { root: true })
      })
  },
  login: async (context, params) => {
    await oAxios.post('Account/CheckLogin', params)
      .then(response => {
        if (response.data.success) {
          let user = {
            firstname: response.data.data.FirstName,
            userId: response.data.data.Id,
            tenantId: response.data.data.TenantId,
          }

          Cookies.set('login', user)

          context.commit('updateAuth', true)
          context.commit('updateLogin', user)

          context.dispatch('app/setMessageSuccess', response.data.message.success, { root: true })
        } else {
          context.dispatch('app/setMessageError', response.data.message.error, { root: true })
        }
      }).catch(error => {
        context.dispatch('app/setMessageError', error.response.data.error_description, { root: true })
      })
  },
  syncLogin: (context) => {
    let user = Cookies.getJSON('login')
    if (user) {
      context.commit('updateAuth', true)
      context.commit('updateLogin', user)
    }
  },
  logout: (context) => {
    context.commit('updateToken', null);
    context.commit('updateAuth', false);
    context.dispatch('setMessageSuccess', 'Logout Success');
  },
  updateTenantCode: (context) => {
    oAxios.post('/api/tenantcode', { 'url': '' })
      .then(response => {
        var data = response.data;
        if (data.message.status) {
          context.commit('updateTenantCode', data.data.tenant_code);
        }
      }).catch(error => {
        context.dispatch('setMessageError', error.response.data.error_description);
      });
  }
}