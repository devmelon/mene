﻿
export default {
  updateAuth (state, status) {
    state.isAuthenticated = status
  },
  updateLogin (state, user) {
    state.user = user
  },
}
