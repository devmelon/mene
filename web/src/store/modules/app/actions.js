﻿// helper function

export default {
  // loading
  showLoading: (context, progress) => {
    context.commit('setLoading', true);

    let p = parseInt(progress)
    if (p > -1) context.commit('setProgress', p)
  },
  hideLoading: context => {
    context.commit('setLoading', false);
  },

  // error message
  setMessage: (context, error) => {
    context.commit('setMessage', error);
    
    setTimeout(() => {
      context.dispatch('removeMessage')
    }, 5000)
  },
  setMessageError: (context, message) => {
    var error = {
      type: 'danger',
      message: message
    }

    context.dispatch('setMessage', error)
  },
  setMessageWarning: (context, message) => {
    var error = {
      type: 'warning',
      message: message
    }

    context.dispatch('setMessage', error)
  },
  setMessageInfo: (context, message) => {
    var error = {
      type: 'info',
      message: message
    }

    context.dispatch('setMessage', error)
  },
  setMessageSuccess: (context, message) => {
    var error = {
      type: 'success',
      message: message
    }

    context.dispatch('setMessage', error)
  },
  removeMessage: (context) => {
    context.commit('setMessage', null);
  }
}