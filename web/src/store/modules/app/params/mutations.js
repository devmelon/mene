﻿import oAxios from '@/connectors/APIConnector'

export default {
  regisTenantTypes (state, data) {
    state.tenantTypes = data;
  },
  preparePersonUpdate (state, data) {
    state.genders = data.Gender;
    state.maritalStatuses = data.MaritalStatus;
  },
}
