﻿// library
import oAxios from '@/connectors/APIConnector'

// helper function

export default {
  // loading
  regisTenantTypes: async context => {
    await oAxios.get('Tenant/GetTenantTypes')
      .then(response => {
        if (response.data.success) {
          context.commit('regisTenantTypes', response.data.data)
        } else {
          console.log("failed get tenant type list");
        }
      }).catch(error => {
        context.dispatch('app/setMessageError', error.response.data.error_description, { root: true });
      })
  },
  preparePersonUpdate: async context => {
    await oAxios.get('Person/PrepareUpdatePerson', { params: { TenantId: context.rootState.auth.user.tenantId } })
      .then(response => {
        if (response.data.success) {
          context.commit('preparePersonUpdate', response.data.data)
        } else {
          console.log("failed to prepare person update");
        }
      }).catch(error => {
        context.dispatch('app/setMessageError', error.response.data.error_description, { root: true });
      })
  },
}