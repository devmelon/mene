﻿export default {
  loading: {
    status: false,
    progress: 1,
  },
  activeURL: null,
  error: {
    status: false,
    type: null,
    message: null
  }
}
