﻿// import { debug } from "util";

export default {
  setMessage (state, error) {
    if (error) {
      state.error = {
        status: true,
        type: error.type,
        message: error.message
      };
    } else {
      state.error = {
        status: false,
        type: null,
        message: null
      };
    }
  },
  setLoading (state, loading) {
    state.loading.status = loading
  },
  setProgress (state, progress) {
    state.loading.progress = progress
  }
}
