// library
import Vue from 'vue'
import Vuex from 'vuex'
// import createPersistedState from 'vuex-persistedstate'

// components
import authModule from './modules/auth/index'
import appModule from './modules/app/index'
// import postModule from './modules/post/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: authModule,
    app: appModule,
  },
  // plugins: [createPersistedState()]
})