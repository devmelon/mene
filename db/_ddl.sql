CREATE TABLE TenantType (
    Id tinyint(2) NOT NULL PRIMARY KEY,
    Name varchar(50) NOT NULL
);

CREATE TABLE RecordStatus (
    Id tinyint(1) NOT NULL PRIMARY KEY,
    Name varchar(50) NOT NULL
);

CREATE TABLE Gender (
    Id tinyint(1) NOT NULL PRIMARY KEY,
    Name varchar(20) NOT NULL
);

CREATE TABLE MaritalStatus (
    Id tinyint(1) NOT NULL PRIMARY KEY,
    Name varchar(20) NOT NULL
);

CREATE TABLE Country (
    Id int(11) NOT NULL PRIMARY KEY,
    Name varchar(50) NOT NULL
);

CREATE TABLE Province (
    Id int(11) NOT NULL PRIMARY KEY,
    CountryId int(11) NOT NULL,
    Name varchar(50) NOT NULL,
    FOREIGN KEY (CountryId) REFERENCES Country(Id)
);

CREATE TABLE City (
    Id int(11) NOT NULL PRIMARY KEY,
    ProvinceId int(11) NOT NULL,
    Name varchar(50) NOT NULL
);

CREATE TABLE Tenant (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantTypeId tinyint(2) NULL,
    AddressId int(11) NULL,
    Name varchar(200) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantTypeId) REFERENCES TenantType(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id)
);

CREATE TABLE AccessRight (
    Id int(11) NOT NULL PRIMARY KEY,
    Name varchar(50) NOT NULL
);

CREATE TABLE Role (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    Name varchar(50) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id)
);

CREATE TABLE RoleAccessRight (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    RoleId int(11) NOT NULL,
    AccessRightId int(11) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id)
);

CREATE TABLE `User` (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    RoleId int(11) NOT NULL,
    FirstName varchar(100) NOT NULL,
    LastName varchar(100) NOT NULL,
    EmailAddress varchar(100) NOT NULL,
    Password text NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (RoleId) REFERENCES Role(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id)
);

CREATE TABLE Address (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    TenantId int(11) NOT NULL,
    Address1 text NOT NULL,
    Address2 text NOT NULL,
    PhoneNumber varchar(100) NOT NULL,
    CityId int(11) NOT NULL,
    ProvinceId int(11) NOT NULL,
    CountryId int(11) NOT NULL,
    ZipCode varchar(100) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (CityId) REFERENCES City(Id),
    FOREIGN KEY (ProvinceId) REFERENCES City(Id),
    FOREIGN KEY (CountryId) REFERENCES Country(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE Feature (
    Id int(11) NOT NULL PRIMARY KEY,
    Name varchar(50) NOT NULL
);

CREATE TABLE TenantFeature (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    FeatureId int(11) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (FeatureId) REFERENCES Feature(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE NoteType (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    FeatureId int(11) NOT NULL,
    Name varchar(200) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (FeatureId) REFERENCES Feature(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE Note (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    NoteTypeId int(11) NOT NULL,
    Description text NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (NoteTypeId) REFERENCES NoteType(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE Ledger (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ParentId int(11) NULL,
    TenantId int(11) NOT NULL,
    Title varchar(200) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (ParentId) REFERENCES Ledger(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE LedgerNote (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    LedgerId int(11) NOT NULL,
    NoteId int(11) NOT NULL,
    FOREIGN KEY (LedgerId) REFERENCES Ledger(Id),
    FOREIGN KEY (NoteId) REFERENCES Note(Id)
);

CREATE TABLE LedgerTransaction (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    LedgerId int(11) NOT NULL,
    TotalCash decimal(13,2) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (LedgerId) REFERENCES Ledger(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE LedgerTransactionNote (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    LedgerTransactionId int(11) NOT NULL,
    NoteId int(11) NOT NULL,
    FOREIGN KEY (LedgerTransactionId) REFERENCES LedgerTransaction(Id),
    FOREIGN KEY (NoteId) REFERENCES Note(Id)
);

CREATE TABLE Event (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    LedgerId int(11) NULL,
    Name varchar(200) NOT NULL,
    Description text NULL,
    StartEvent datetime NULL,
    EndEvent datetime NULL,
    Location varchar(200) NULL,
    Speaker varchar(200) NULL,
    Topic varchar(200) NULL,
    TotalAttendants int(11) NULL,
    Evaluation text NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (LedgerId) REFERENCES Ledger(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE EventNote (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    EventId int(11) NOT NULL,
    NoteId int(11) NOT NULL,
    FOREIGN KEY (EventId) REFERENCES Event(Id),
    FOREIGN KEY (NoteId) REFERENCES Note(Id)
);

CREATE TABLE Person (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    AddressId int(11) NULL,
    FirstName varchar(200) NOT NULL,
    LastName varchar(200) NOT NULL,
    GenderId tinyint(1)  NULL,
    BirthPlace varchar(200) NULL,
    BirthDate datetime NULL,
    BloodType varchar(20) NULL,
    EmailAddress varchar(100) NULL,
    PhoneNumber varchar(100) NULL,
    MaritalStatusId tinyint(1) NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (AddressId) REFERENCES Address(Id),
    FOREIGN KEY (GenderId) REFERENCES Gender(Id),
    FOREIGN KEY (MaritalStatusId) REFERENCES MaritalStatus(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE PersonNote (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    PersonId int(11) NOT NULL,
    NoteId int(11) NOT NULL,
    FOREIGN KEY (PersonId) REFERENCES Person(Id),
    FOREIGN KEY (NoteId) REFERENCES Note(Id)
);

CREATE TABLE InventoryCategory (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    Name varchar(200) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE InventoryItem (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    Name varchar(200) NOT NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

CREATE TABLE InventoryItemCategory (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    InventoryItemId int(11) NOT NULL,
    InventoryCategoryId int(11) NOT NULL,
    FOREIGN KEY (InventoryItemId) REFERENCES InventoryItem(Id),
    FOREIGN KEY (InventoryCategoryId) REFERENCES InventoryCategory(Id)
);

CREATE TABLE Inventory (
    Id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TenantId int(11) NOT NULL,
    ItemId int(11) NOT NULL,
    Total decimal(10,2) NOT NULL,
    NoteId int(11) NULL,
    RecordStatusId tinyint(1) NOT NULL,
    CreatedOn datetime NOT NULL,
    CreatedBy int(11) NOT NULL,
    UpdatedOn datetime NULL,
    UpdatedBy int(11) NULL,
    FOREIGN KEY (TenantId) REFERENCES Tenant(Id),
    FOREIGN KEY (ItemId) REFERENCES InventoryItem(Id),
    FOREIGN KEY (NoteId) REFERENCES Note(Id),
    FOREIGN KEY (RecordStatusId) REFERENCES RecordStatus(Id),
    FOREIGN KEY (CreatedBy) REFERENCES User(Id),
    FOREIGN KEY (UpdatedBy) REFERENCES User(Id)
);

/* Add Constraints */

ALTER TABLE Tenant
ADD FOREIGN KEY (AddressId) REFERENCES Address(Id);

ALTER TABLE Tenant
ADD FOREIGN KEY (CreatedBy) REFERENCES User(Id);

ALTER TABLE Tenant
ADD FOREIGN KEY (UpdatedBy) REFERENCES User(Id);

ALTER TABLE Role
ADD FOREIGN KEY (CreatedBy) REFERENCES User(Id);

ALTER TABLE Role
ADD FOREIGN KEY (UpdatedBy) REFERENCES User(Id);

ALTER TABLE RoleAccessRight
ADD FOREIGN KEY (CreatedBy) REFERENCES User(Id);

ALTER TABLE `User`
ADD FOREIGN KEY (CreatedBy) REFERENCES User(Id);

ALTER TABLE `User`
ADD FOREIGN KEY (UpdatedBy) REFERENCES User(Id);


